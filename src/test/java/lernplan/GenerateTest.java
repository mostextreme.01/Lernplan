package lernplan;

import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.Iterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class GenerateTest {

    static TaskNode root = new TaskNode();

    @Test
    @Order(1)
    void check() {

        try {
            root = new TaskNode();
            // initialize tree with dummy values
            root.addChild("node1", 55);
            TaskNode node2 = root.addChild("node2", 5);
            node2.addChild("node21", 0);  // 50
            node2.addChild("node22", 0);  // 50
            TaskNode node3 = root.addChild("node3", 0);     // 40
            node3.addChild("node31", 90);
            node3.addChild("node32", 0);  // 5
            node3.addChild("node33", 0);  // 5

            Generate.check(root);
        }
        catch(Exception e){
            Assertions.fail();
        }


        {
            TaskNode n = new TaskNode();
            n.addChild("node1", 99);
            n.addChild("node2", 2);
            Assertions.assertThrows(IllegalArgumentException.class, () -> Generate.check(n));
        }

        {
            TaskNode n = new TaskNode();
            n.addChild("node1", 99);
            Assertions.assertThrows(IllegalArgumentException.class, () -> Generate.check(n));
        }

        {
            TaskNode n = new TaskNode();
            n.addChild("node1", 100);
            n.addChild("node2", 0);
            Assertions.assertThrows(IllegalArgumentException.class, () -> Generate.check(n));
        }


    }

    @Test
    @Order(3)
    void start() {
        Generate.start(root, 30);

        Iterator<TaskNode> i = root.iterator();
        // Node 1
        Assertions.assertTrue(i.hasNext());
        TaskNode node = i.next();
        Assertions.assertEquals(16.5, node.getDays(), 0.0001);

        // Node 2
        Assertions.assertTrue(i.hasNext());
        node = i.next();
        Assertions.assertEquals(1.5, node.getDays(), 0.0001);
        Iterator<TaskNode> i2 = node.iterator();
        Assertions.assertTrue(i2.hasNext());
        Assertions.assertEquals(0.75, i2.next().getDays(), 0.0001);
        Assertions.assertTrue(i2.hasNext());
        Assertions.assertEquals(0.75, i2.next().getDays(), 0.0001);
        Assertions.assertFalse(i2.hasNext());

        // Node 3
        Assertions.assertTrue(i.hasNext());
        node = i.next();
        Assertions.assertEquals(12.0, node.getDays(), 0.0001);
        i2 = node.iterator();
        Assertions.assertTrue(i2.hasNext());
        Assertions.assertEquals(10.8, i2.next().getDays(), 0.0001);
        Assertions.assertTrue(i2.hasNext());
        Assertions.assertEquals(0.6, i2.next().getDays(), 0.0001);
        Assertions.assertTrue(i2.hasNext());
        Assertions.assertEquals(0.6, i2.next().getDays(), 0.0001);
        Assertions.assertFalse(i2.hasNext());

        Assertions.assertFalse(i.hasNext());


        {
            try {
                TaskNode taskNode = new TaskNode();
                // initialize tree with dummy values
                taskNode.addChild("node1", 55, true);
                TaskNode node2 = taskNode.addChild("node2", 5);
                node2.addChild("node21", 50);  // 50
                node2.addChild("node22", 50, true);  // 50
                TaskNode node3 = taskNode.addChild("node3", 40);     // 40
                node3.addChild("node31", 90, true);
                node3.addChild("node32", 5);  // 5
                node3.addChild("node33", 5);  // 5

                Generate.check(taskNode);
                Generate.removeIgnoreNodes(taskNode);
                Generate.start(taskNode, 100);

                Stream<TaskNode> stream = StreamSupport.stream(taskNode.spliterator(), false);
                double[] p = stream.mapToDouble(TaskNode::getDays).toArray();
                double[] exp = {0, 38.46, 61.53};
                Assertions.assertArrayEquals(exp, p, 0.01);

                stream = StreamSupport.stream(node2.spliterator(), false);
                p = stream.mapToDouble(TaskNode::getDays).toArray();
                exp = new double[]{38.46, 0};
                Assertions.assertArrayEquals(exp, p, 0.01);

                stream = StreamSupport.stream(node3.spliterator(), false);
                p = stream.mapToDouble(TaskNode::getDays).toArray();
                exp = new double[]{0, 30.76, 30.76};
                Assertions.assertArrayEquals(exp, p, 0.01);
            }
            catch(Exception e){
                System.out.println(e.getMessage());
                Assertions.fail();
            }
        }
    }

    @Test
    @Order(2)
    void removeIgnoreNodes() {
        System.out.println("removeIgnoreNodes");
        try
        {
            {
                TaskNode n = new TaskNode();
                n.addChild("node1", 50);
                n.addChild("node2", 25, true);
                n.addChild("node3", 25);
                Generate.removeIgnoreNodes(n);

                Stream<TaskNode> stream = StreamSupport.stream(n.spliterator(), false);
                double[] p = stream.mapToDouble(TaskNode::getPercentage).toArray();
                double[] exp = {66.66, 0, 33.33};
                Assertions.assertArrayEquals(exp, p, 0.01);
                double pTotal = Arrays.stream(p).sum();
                Assertions.assertEquals(100, pTotal, 0.001);
            }

            {
                TaskNode n = new TaskNode();
                TaskNode node1 = n.addChild("node1", 50);
                node1.addChild("node11", 50);
                node1.addChild("node12", 50, true);
                n.addChild("node2", 25);
                n.addChild("node3", 25);
                Generate.removeIgnoreNodes(n);

                Stream<TaskNode> stream = StreamSupport.stream(n.spliterator(), false);
                double[] p = stream.mapToDouble(TaskNode::getPercentage).toArray();
                double[] exp = {33.33, 33.33, 33.33};
                Assertions.assertArrayEquals(exp, p, 0.01);

                Stream<TaskNode> snode1 = StreamSupport.stream(node1.spliterator(), false);
                p = snode1.mapToDouble(TaskNode::getPercentage).toArray();
                exp = new double[]{100, 0};
                Assertions.assertArrayEquals(exp, p, 0.01);

                Assertions.assertEquals(100, Arrays.stream(p).sum(), 0.001);
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
            Assertions.fail();
        }

        {
            // Sonderfall: letzter existierender Punkt der Ebene wird ignoriert
            TaskNode n = new TaskNode();
            n.addChild("node1", 100, true);
            Assertions.assertThrows(ArithmeticException.class, () -> Generate.removeIgnoreNodes(n));
        }


    }
}