package lernplan;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

class TaskNodeTest {

    final TaskNode root = new TaskNode();

    @Test
    void addChild() {
        TaskNode node1 = root.addChild("node1", 0);
        Assertions.assertEquals(node1.getPercentage(), 0);
        Assertions.assertEquals(node1.getText(), "node1");

        TaskNode node2 = root.addChild("node2", 0);
        Assertions.assertEquals(node2.getPercentage(), 0);
        Assertions.assertEquals(node2.getText(), "node2");

        TaskNode node21 = node2.addChild("node21", 0);
        Assertions.assertEquals(node21.getPercentage(), 0);
        Assertions.assertEquals(node21.getText(), "node21");

        TaskNode node22 = node2.addChild("node22", 0);
        Assertions.assertEquals(node22.getPercentage(), 0);
        Assertions.assertEquals(node22.getText(), "node22");

        TaskNode node3 = root.addChild("node3", 0);
        Assertions.assertEquals(node3.getPercentage(), 0);
        Assertions.assertEquals(node3.getText(), "node3");

        TaskNode node31 = node3.addChild("node31", 0);
        Assertions.assertEquals(node31.getPercentage(), 0);
        Assertions.assertEquals(node31.getText(), "node31");

        TaskNode node32 = node3.addChild("node32", 0);
        Assertions.assertEquals(node32.getPercentage(), 0);
        Assertions.assertEquals(node32.getText(), "node32");

        TaskNode node33 = node3.addChild("node33", 0);
        Assertions.assertEquals(node33.getPercentage(), 0);
        Assertions.assertEquals(node33.getText(), "node33");


        Iterator<TaskNode> i = root.iterator();
        // Node 1
        Assertions.assertTrue(i.hasNext());
        TaskNode node = i.next();
        Assertions.assertEquals(node.getText(), "node1");

        // Node 2
        Assertions.assertTrue(i.hasNext());
        node = i.next();
        Assertions.assertEquals(node.getText(), "node2");
        Iterator<TaskNode> i2 = node.iterator();
        Assertions.assertTrue(i2.hasNext());
        Assertions.assertEquals(i2.next().getText(), "node21");
        Assertions.assertTrue(i2.hasNext());
        Assertions.assertEquals(i2.next().getText(), "node22");
        Assertions.assertFalse(i2.hasNext());

        // Node 3
        Assertions.assertTrue(i.hasNext());
        node = i.next();
        Assertions.assertEquals(node.getText(), "node3");
        i2 = node.iterator();
        Assertions.assertTrue(i2.hasNext());
        Assertions.assertEquals(i2.next().getText(), "node31");
        Assertions.assertTrue(i2.hasNext());
        Assertions.assertEquals(i2.next().getText(), "node32");
        Assertions.assertTrue(i2.hasNext());
        Assertions.assertEquals(i2.next().getText(), "node33");
        Assertions.assertFalse(i2.hasNext());

        Assertions.assertFalse(i.hasNext());

    }
}