package lernplan;

import org.apache.commons.cli.ParseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ParseCmdLineTest {

    @Test
    void parse() {
        ParseCmdLine params;

        {
            String[] args = {"-o", "ofil", "-i", "ifil", "-d", "11"};
            try{
                params = new ParseCmdLine(args);
                Assertions.assertEquals("ifil", params.getInFile());
                Assertions.assertEquals("ofil", params.getOutFile());
                Assertions.assertEquals(11, params.getDays());
            }
            catch(Exception e){
                Assertions.fail();
            }
        }

        {
            String[] args = {"-d", "abc"};
            Assertions.assertThrows(NumberFormatException.class, () -> new ParseCmdLine(args));
        }

        {
            String[] args = {"-fail"};
            Assertions.assertThrows(ParseException.class, () -> new ParseCmdLine(args));
        }

        {
            String[] args = {"-o"};
            Assertions.assertThrows(ParseException.class, () -> new ParseCmdLine(args));
        }

        {
            String[] args = {"-version"};
            try{
                params = new ParseCmdLine(args);
                Assertions.assertEquals(0, params.getDays());
            }
            catch(Exception e){
                Assertions.fail();
            }
        }

        {
            String[] args = {"-help"};
            try{
                params = new ParseCmdLine(args);
                Assertions.assertEquals(0, params.getDays());
            }
            catch(Exception e){
                Assertions.fail();
            }
        }

        {
            String[] args = {};
            Assertions.assertThrows(ParseException.class, () -> new ParseCmdLine(args));
        }

        {
            String[] args = {"-d", "0"};
            Assertions.assertThrows(ParseException.class, () -> new ParseCmdLine(args));
        }





    }
}