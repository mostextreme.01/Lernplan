package lernplan;

import java.util.ArrayList;
import java.util.Iterator;

public class TaskNode implements Iterable<TaskNode> {
    private float percentage = 0;
    private String text;
    private final ArrayList<TaskNode> children = new ArrayList<>();
    private TaskNode parent;
    private float days;
    private boolean ignore = false;


    public TaskNode(){

    }

    public TaskNode(String text, float percentage){
        this();
        this.text = text;
        this.percentage = percentage;
    }

    public TaskNode(String text, float percentage, boolean ignore){
        this(text, percentage);
        this.ignore = ignore;
    }

    public TaskNode addChild(String text, float percentage){
        return addChild(text, percentage, false);
    }

    public TaskNode addChild(String text, float percentage, boolean ignore){
        TaskNode childTaskNode = new TaskNode(text, percentage, ignore);
        childTaskNode.parent = this;
        this.children.add(childTaskNode);
        return childTaskNode;
    }


    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    public String getText() {
        return text;
    }

    @Override
    public Iterator<TaskNode> iterator() {
        return children.iterator();
    }

    public int level(){
        if(parent == null){
            return 0;
        }
        else{
            return parent.level() + 1;
        }
    }

    public float getDays() {
        return days;
    }

    public void setDays(float days) {
        this.days = days;
    }

    public boolean isEmpty(){
        return children.size() == 0;
    }

    public boolean isIgnore() {
        return ignore;
    }
}
