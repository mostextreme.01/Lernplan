package lernplan;

import org.apache.commons.cli.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ParseCmdLine {
    private String outFile;
    private String inFile;
    private int days;

    public ParseCmdLine(){

    }

    public ParseCmdLine( String[] args ) throws ParseException, IOException
    {
        this();
        parse(args);
    }

    public void parse( String[] args ) throws ParseException, IOException
    {
        days = 0;
        outFile = null;
        inFile = null;

        // Parameter options
        Option outfileOpt = new Option("o", true, "output file");
        Option infileOpt = new Option("i", true, "input file");
        Option daysOpt = new Option("d", true, "number of days");
        Option help = new Option("help", "print this message");
        Option version = new Option("version", "print the version information and exit");

        Options options = new Options();
        options.addOption(help);
        options.addOption(version);
        options.addOption(outfileOpt);
        options.addOption(infileOpt);
        options.addOption(daysOpt);

        CommandLineParser parser = new DefaultParser();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        try(InputStream resourceStream = loader.getResourceAsStream("application.properties")) {
            CommandLine cmd = parser.parse(options, args);

            if (cmd.hasOption(help)){
                // automatically generate the help statement
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("lernplan", options);
            }
            else if(cmd.hasOption(version)){
                final Properties properties = new Properties();
                properties.load(resourceStream);
                System.out.println(properties.getProperty("application.version"));
            }
            else{
                outFile = cmd.getOptionValue(outfileOpt);
                inFile = cmd.getOptionValue(infileOpt);

                if( !cmd.hasOption(daysOpt) ){
                    throw new ParseException("Missing mandatory option -d");
                }
                days = Integer.parseInt( cmd.getOptionValue(daysOpt) );
                if(days == 0){
                    throw new ParseException("Invalid value for option -d: 0");
                }
            }
        }
        catch (ParseException e) {
            System.out.println("Please, follow the instructions below:");
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("lernplan", options);
            throw e;
        }
        catch (IOException e) {
            System.err.println("Loading ressources failed.  Reason: " + e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

    public String getOutFile() {
        return outFile;
    }

    public String getInFile() {
        return inFile;
    }

    public int getDays() {
        return days;
    }
}
