package lernplan;

import org.apache.commons.cli.ParseException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        try{
            ParseCmdLine params = new ParseCmdLine(args);

            // initialize tree with dummy values
            TaskNode root = new TaskNode();
            root.addChild("node1", 55);
            TaskNode node2 = root.addChild("node2", 5);
            node2.addChild("node21", 0);  // 50
            node2.addChild("node22", 0);  // 50
            TaskNode node3 = root.addChild("node3", 0);     // 40
            node3.addChild("node31", 90);
            node3.addChild("node32", 0);  // 5
            node3.addChild("node33", 0);  // 5

            if( params.getDays() > 0 ){
                Generate.check(root);
                Generate.start(root, params.getDays());
            }
        }
        catch( NumberFormatException e){
            System.err.println("Parsing failed. Number Format Exception " + e.getMessage());
        }
        catch(IllegalArgumentException e){
            System.err.println("Invalid data in: " + e.getMessage());
        }
        catch (ParseException e) {
            System.err.println("Parsing failed.  Reason: " + e.getMessage());
        }
        catch(Exception e){
            System.err.println(e.getMessage());
        }


    }
}
