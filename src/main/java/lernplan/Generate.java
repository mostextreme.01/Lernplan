package lernplan;

import java.util.function.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public final class Generate {
    private static final float delta = 0.001f;

    public static void start(TaskNode root, float days){

        for (TaskNode t : root) {
            float tDays = days * t.getPercentage() / 100;
            t.setDays(tDays);
            System.out.println(" ".repeat(t.level() * 2) +  t.getText() + ": " + tDays);
            start(t, tDays);
        }
    }

    public static void check(TaskNode root){
        if(root.isEmpty()){
            return;
        }

        float percentage = 0;
        int cnt = 0;

        for (TaskNode t : root) {
            if( Math.abs(t.getPercentage()) < delta){
                cnt++;
            }
            else{
                percentage += t.getPercentage();
            }
            check(t);
        }

        if( percentage > 100 + delta ){
            // Fehler: zu viel Prozente
            throw new IllegalArgumentException(root.getText() + ": Sum > 100%: " + percentage);
        }

        float rest = Math.abs( percentage - 100 );

        if( rest > delta){
            // Hier fehlt noch etwas
            if(cnt == 0){
                // Fehler: Es muss aufgefuellt werden, aber es gibt keine Eintraege
                throw new IllegalArgumentException(root.getText() + ": Sum < 100%: "  + percentage);
            }
            else{
                float percentagePerDay = rest / cnt;
                for (TaskNode t : root) {
                    if(t.getPercentage() == 0){
                        t.setPercentage(percentagePerDay);
                    }
                }
            }
        }
        else{
            if(cnt > 0){
                // Fehler: 100% aber es gibt Eintraege mit 0
                throw new IllegalArgumentException(root.getText() + ": Entries with 0%" );
            }
        }
    }

    public static float removeIgnoreNodes(TaskNode root){
        if(root.isEmpty()){
            return 0;
        }

        float nestedPercentage = 0;

        for (TaskNode taskNode : root) {
            float p = taskNode.getPercentage() * removeIgnoreNodes(taskNode) / 100;
            if( p > delta){
                // Hier: es gab eine Aenderung
                taskNode.setPercentage(taskNode.getPercentage() - p);
                nestedPercentage += p;
            }
        }

        Supplier<Stream<TaskNode>> stream = () -> StreamSupport.stream( root.spliterator(), false);
        //Stream<TaskNode> stream = StreamSupport.stream( root.spliterator(), false);
        Predicate<TaskNode> isIgnored = TaskNode::isIgnore;
        Predicate<TaskNode> isNotIgnored = taskNode -> !taskNode.isIgnore();
        final float ignoredPercentage = (float)stream.get().filter(isIgnored).mapToDouble(TaskNode::getPercentage).sum() + nestedPercentage;
        final float percentageUsed = (float)stream.get().filter(isNotIgnored).mapToDouble(TaskNode::getPercentage).sum();
        stream.get().filter(isIgnored).forEach(e -> e.setPercentage(0));
        stream.get().filter(isNotIgnored).forEach(e -> e.setPercentage( e.getPercentage() + ( ignoredPercentage * e.getPercentage() / percentageUsed )));

        // little logical check
        double p = stream.get().mapToDouble(TaskNode::getPercentage).sum();
        if( Math.abs(p - 100) > delta ) throw new ArithmeticException("less than 100% after remove ignore nodes in: " + root.getText());

        return ignoredPercentage;

    }





}
